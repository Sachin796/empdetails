from dotenv import load_dotenv
import os
load_dotenv()


def GET_DEVENV(var, default=None):
    return os.getenv(var) or default


config = {
    'DB_URL': GET_DEVENV('DB_URL')
}
