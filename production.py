import os


def GET_PRODENV(var, default=None):
    return os.environ.get(var) or default


config = {
    'DB_URL': GET_PRODENV('DB_URL')
}
